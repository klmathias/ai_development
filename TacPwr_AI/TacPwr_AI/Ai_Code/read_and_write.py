# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 15:31:39 2021

@author: hcollins
"""

import sched, time
import sqlite3
import os
import pickle
import json
import pandas as pd
import socket

from check_inrush import check_for_inrush


def setup_defaults():
    ai_dir = os.path.abspath("/home/kevin/TacPwrPackage/TacPwr_AI/Ai_Code")
    db_path = os.path.abspath("/home/kevin/TacPwrPackage/controller/TacPwrData/db/tacpwr.db")
    TCP_IP = '127.0.0.1'
    TCP_PORT = 8070
    
    global data_dir, ai_model, tcp_socket, db_connection, db_cursor, priority_dict, first_length, data_cols, watched_cols
    data_dir = os.path.abspath("/home/kevin/TacPwrPackage/controller")
    ai_model = pickle.load(open(os.path.join(ai_dir, "finalized_model.sav"), 'rb'))
    rp = False
    while rp == False:
        try:
            tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            tcp_socket.connect((TCP_IP, TCP_PORT))
            ai_id = (133).to_bytes(8, 'little')
            ipdu_id = (0).to_bytes(8, 'little')
            message_id = (0).to_bytes(4, 'little')
            msg_cmd = b'HB00'
            msg_len = (0).to_bytes(4, 'little')
            
            tcp_msg = ai_id + ipdu_id + message_id + msg_cmd + msg_len
            tcp_socket.send(tcp_msg)
            print("1 Heart Beat Message Sent to Controller.")
            rp = True
        except:
            raise Exception('ERROR: Ran into a tcp connection issue during heartbeat.')
            rp = False
    
    db_connection = sqlite3.connect(db_path)
    db_cursor = db_connection.cursor()
   
    priority_dict = json.load(open(os.path.join(ai_dir, "priority_dictioary.json"), 'r'))
    
    first_length = len(read_datatable())
    data_cols = ['Voltage L1', 
                'Voltage L2', 
                'Voltage L3', 
                'Noise', 
                'MAIN_L1_AMP', 
                'MAIN_L2_AMP', 
                'MAIN_L3_AMP', 
                'MAIN_N_AMP',  
                'F60A_L1_AMP', 
                'F60A_L2_AMP', 
                'F60A_L3_AMP', 
                'F60A_N_AMP', 
                'F60B_L1_AMP', 
                'F60B_L2_AMP', 
                'F60B_L3_AMP', 
                'F60B_N_AMP', 
                'F40A_L1_AMP', 
                'F40A_L2_AMP', 
                'F40A_L3_AMP', 
                'F40A_N_AMP', 
                'F40B_L1_AMP', 
                'F40B_L2_AMP', 
                'F40B_L3_AMP', 
                'F40B_N_AMP', 
                'F20A_L1_AMP', 
                'F20A_N_AMP', 
                'F20B_L2_AMP', 
                'F20B_N_AMP', 
                'FBYP_L1_AMP', 
                'FBYP_L2_AMP', 
                'FBYP_L3_AMP', 
                'FBYP_N_AMP', 
                'DEADBEEF', 
                'timeHigh',
                'timeLow',
                'timeNano']
    watched_cols = ['F60A_L1_AMP', 
                    'F60B_L1_AMP',  
                    'F40A_L1_AMP',  
                    'F40B_L1_AMP',  
                    'F20A_L1_AMP', 
                    'F20B_L2_AMP']

def read_datatable():
    try:    
        db_cursor.execute("""SELECT * FROM MESSAGE;""")
        result = db_cursor.fetchall()
        return(result)
    except:    
        raise Exception('ERROR: Ran into a sql connection issue within the FAST table.')

def write_to_tcp(data):
    repeat = False
    while repeat == False:  
        try:
            ai_id = (133).to_bytes(8, 'little')
            ipdu_id = (data[0]).to_bytes(8, 'little')
            message_id = (0).to_bytes(4, 'little')
            msg_cmd = b'SD00'
            msg_len = (24).to_bytes(4, 'little')
            
            tcp_msg = ai_id + ipdu_id + message_id + msg_cmd + msg_len
            for circuit_data in data[1:]:
                tcp_msg = tcp_msg + (circuit_data).to_bytes(4, 'little')
    
            tcp_socket.send(tcp_msg)
            print("1 Set Feed Shedding Priority Message Sent to Controller.")
            repeat = True
        except:
            raise Exception('ERROR: Ran into a tcp connection issue during set feed shedding priority message.')
            repeat == False
        
def write_to_database(data):
    try:
        sql = """INSERT INTO OUTPUT (IPDU_ID, main_60A_priority, main_60B_priority, main_40A_priority, main_40B_priority, main_20A_priority, main_20B_priority) VALUES (?, ?, ?, ?, ?, ?, ?);"""
        db_cursor.execute(sql, data)
    
        db_connection.commit()
        print(db_cursor.rowcount, "record inserted.")
    except:
        raise Exception('ERROR: Ran into a sql connection issue within the OUTPUT table.')
    
def read_data_file(data_path):
    realtime_df = pd.read_csv(data_path, skiprows = 1, header = None).drop([36], axis=1).set_axis(data_cols, axis=1)
    triggering_cols = list(pd.read_csv(data_path, nrows = 1))
    cols_of_interest = list(set([col.strip() for col in triggering_cols]) & set(watched_cols))
    if len(cols_of_interest) == 0:
        return({'Error': "There is no in-rush event in the data"})
    else:
        return({'Data': realtime_df, 'Cols': cols_of_interest})

def get_sampling_rate(first_time, second_time): 
    seconds_difference = ((second_time[0] << 32) + second_time[1]) - ((first_time[0] << 32) + first_time[1])
    if seconds_difference == 0:
        sampling_rate = 1/((second_time[2]/10**9) - (first_time[2]/10**9))
    elif seconds_difference == 1:
        sampling_rate = 1/((1+(second_time[2]/10**9)) - (first_time[2]/10**9))
    return(sampling_rate)

def extract_inrush_data(data_dict):
    if data_dict.get('Error', False) == False:
        realtime_df = data_dict["Data"]
        cols_of_interest = data_dict["Cols"]
        
        first_time = realtime_df.loc[0][['timeHigh', 'timeLow', 'timeNano']]
        second_time = realtime_df.iloc[1][['timeHigh', 'timeLow', 'timeNano']]
        sampling_rate = get_sampling_rate(first_time, second_time)
        
        inrush_data = {}
        for circuit in cols_of_interest:
            inrush_data[circuit.split('_')[0][1:]] = check_for_inrush(sampling_rate, realtime_df[circuit])
        return({'Data': inrush_data})
    else:
        return({'Error': data_dict})

def get_priority(ipdu, inrush_data, previous_priorities):
    original_priority = previous_priorities.copy()
    if inrush_data.get('Error', False) == False:
        for circuit, data in inrush_data['Data'].items():       
            if data.get('Error', False) == False:
                test_data = pd.DataFrame({0: [pd.Series(data['Current'])]})
                prediction = ai_model.predict(test_data)[0]
                print('The Ai predicted circuit {} of IPDU {} is a {}.'.format(circuit, ipdu, prediction))
                priority = priority_dict[prediction]
                previous_priorities[circuit] = priority
        if original_priority != previous_priorities:
            priority_tpl = tuple([ipdu] + list(previous_priorities.values())) 
            print('Ipdu {} has the following priorities {}'.format(ipdu, previous_priorities))
            write_to_tcp(priority_tpl) 
    return(previous_priorities)
        
def check_database(cycle, initial_length, previous_priorities): 
    result = read_datatable()
    if len(result) != initial_length:
        for file in result[initial_length:]:
            ipdu = file[0]
            data_path = os.path.abspath(os.path.join(data_dir, file[1]))
            data_dict = read_data_file(data_path)
            inrush_events = extract_inrush_data(data_dict)
            previous_priorities = get_priority(ipdu, inrush_events, previous_priorities)
        initial_length = len(result)
    cycle.enter(1, 1, check_database, (cycle, initial_length, previous_priorities))

def run_ai():
    setup_defaults()
    
    code_scheduler = sched.scheduler(time.time, time.sleep)
    initial_priorities = {'60A': 100, 
                    '60B': 100,  
                    '40A': 100,  
                    '40B': 100,  
                    '20A': 100, 
                    '20B': 100}
    code_scheduler.enter(1, 1, check_database, (code_scheduler, first_length, initial_priorities))
    code_scheduler.run()