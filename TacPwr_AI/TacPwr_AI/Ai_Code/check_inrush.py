# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 08:08:35 2021

@author: hcollins
"""

import pandas as pd
from scipy.signal import find_peaks, resample

def variance(data):
    n = len(data)
    mean = sum(data) / n
    deviations = [(x - mean) ** 2 for x in data]
    variance = sum(deviations) / n
    return variance

def standardize_data(data_sample_rate, current, start_value, std_rate = 30000):
    data_length = len(current.tolist())
    
    stand_start = round(start_value - 0.5*data_sample_rate)
    if stand_start < 0:
        stand_start = 0
    stand_end = stand_start + round(data_sample_rate)
    
    if data_length < data_sample_rate:
        added_current_data = []
        for x in range(data_sample_rate - data_length):
            added_current_data.append(0)
        current.append(pd.Series(added_current_data), ignore_index=True)
    
    if round(data_sample_rate) != std_rate:
        std_current = pd.Series(resample(current.iloc[stand_start:stand_end], std_rate))
    else:
        std_current = current.iloc[stand_start:stand_end]
    return(std_current)

def check_for_inrush(data_sample_rate, current, top_threshold_current = 20, bottom_threshold_current = 12):
    peaks, _ = find_peaks(current, prominence = current.max())
    top_threshold_values = current[abs(current) > top_threshold_current].index.tolist()
    bottom_threshold_values = current[abs(current) > bottom_threshold_current].tolist()
    if len(bottom_threshold_values) == 0:
        return({'Error': "There is no in-rush event in the data"})
    if len(top_threshold_values) > 0:
        sample_start = min(top_threshold_values)
        sample_end = max(top_threshold_values)
    if 'sample_start' not in locals():
        if len(peaks) > 0:
            if variance(current.iloc[peaks]) < 1:
                return({'Error': "There is no in-rush event in the data"})
            else:
                std_current = standardize_data(data_sample_rate, current, peaks.min())
                return({'Current': std_current})
        else:
            return({'Error': "There is no in-rush event in the data"})
    else:
        if len(peaks) > 0:
            if variance(current.iloc[peaks]) >= 1:
                if peaks.min() < sample_start:
                    sample_start = peaks.min()
                if peaks.max() > sample_end:
                    sample_end = peaks.max() 
        if abs(current.iloc[0]) < 5*abs(current.iloc[-1]):    
            std_current = standardize_data(data_sample_rate, current, sample_start)
            return({'Current': std_current})  
        else:
            return({'Error': "There is no in-rush event in the data"})