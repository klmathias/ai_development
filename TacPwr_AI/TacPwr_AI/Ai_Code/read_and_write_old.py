# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 11:01:37 2021

@author: hcollins
"""

import sched, time
import sqlite3
import os
import pickle
import json
import pandas as pd
from check_inrush import check_for_inrush


def setup_defaults():
    ai_dir = os.path.abspath("/home/kevin/TacPwrPackage/TacPwr_AI/Ai_Code")
    db_path = os.path.abspath("/home/kevin/TacPwrPackage/controller/TacPwrData/db/tacpwr.db")
    
    global ai_model, db_connection, db_cursor, priority_dict, check_time  
    ai_model = pickle.load(open(os.path.join(ai_dir, "finalized_model.sav"), 'rb'))
    db_connection = sqlite3.connect(db_path)
    db_cursor = db_connection.cursor()
    priority_dict = json.load(open(os.path.join(ai_dir, "priority_dictioary.json"), 'r'))
    check_time = read_datatable()[0][7:]

def read_datatable():
    db_cursor.execute("SELECT IPDU_ID, f60a_l1_amp, f60b_l1_amp, f40a_l1_amp, f40b_l1_amp, f20a_l1_amp, f20b_l2_amp, timeHigh, timeLow, timeNano FROM FAST")
    result = db_cursor.fetchall()
    print(result[0])
    return(result)

def write_to_database(data):
    sql = "INSERT INTO OUTPUT IPDU_ID, main_60A_priority, main_60B_priority, main_40A_priority, main_40B_priority, main_20A_priority, main_20B_priority VALUES (%i, %i, %i, %i, %i, %i, %i)"
    db_cursor.execute(sql, data)

    db_connection.commit()

    print(db_cursor.rowcount, "record inserted.")

def extract_inrush_data(SQL_Query):
    query_df = pd.DataFrame(SQL_Query, columns=["IPDU", "60a_current", "60b_current", "40a_current", "40b_current", "20a_current", "20b_current", "timeHigh", "timeLow", "timeNano"])
    idpu_grouped = query_df.groupby(query_df["IPDU"])
    ipdu_df_dict = {ipdu: idpu_grouped.get_group(ipdu) for ipdu in list(set(list(query_df["IPDU"])))}
    
    inrush_dict = {}
    for ipdus, data_df in ipdu_df_dict.items():
        first_time = data_df.iloc[0][7:]
        second_time = data_df.iloc[1][7:]
        seconds_difference = ((second_time[0] << 32) + second_time[1]) - ((first_time[0] << 32) + first_time[1])
        if seconds_difference == 0:
            sampling_rate = 1/((second_time[2]/10**9) - (first_time[2]/10**9))
        elif seconds_difference == 1:
            sampling_rate = 1/((1+(second_time[2]/10**9)) - (first_time[2]/10**9))
        
        inrush_data = {}
        for circuit in ["60a_current", "60b_current", "40a_current", "40b_current", "20a_current", "20b_current"]:
            inrush_data[circuit.split('_')[0]] = check_for_inrush(sampling_rate, data_df[circuit])
        
        inrush_dict[ipdus] = inrush_data
    
    return(inrush_dict)

def get_priority(inrush_data):
    for ipdu, data in inrush_data.items():
        output_list = ["60a", "60b", "40a", "40b", "20a", "20b"] 
        for circuit, current in data.items():       
            if current.get('Error', False) == False:
                prediction = ai_model.predict(pd.DataFrame(current['Data']['Current']))[0]
                priority = priority_dict[prediction]
            else: 
                priority = -1
            output_list[output_list.index(circuit)] = priority
        if any(v != -1 for v in output_list):
            lowest_priority = max(output_list)
            for pos in range(len(output_list)):
                if output_list[pos] == -1:
                    output_list[pos] = lowest_priority + 1
        priority_tpl = tuple([ipdu] + output_list) 
        write_to_database(priority_tpl)  
        
def check_database(cycle, initial_time): 
    result = read_datatable()
    if result[0][7:] != initial_time:
        inrush_events = extract_inrush_data(result)
        get_priority(inrush_events)
        initial_time = result[0][7:]
    cycle.enter(1, 1, check_database, (cycle, initial_time))

def run_ai():
    setup_defaults()
    
    code_scheduler = sched.scheduler(time.time, time.sleep)
    code_scheduler.enter(1, 1, check_database, (code_scheduler, check_time))
    code_scheduler.run()